import os
import json
import time

from googletrans import Translator
translator=Translator()
#set_default_proxies({'http': '127.0.0.1:1080'})

#PATHS
resPath=os.path.join(os.path.dirname(os.path.realpath(__file__)),'masterdata')
p_Path=os.path.join(resPath,'parsed')
t_Path=os.path.join(resPath,'translation')
td_Path=os.path.join(resPath,'translation_patch')


os.makedirs(t_Path,exist_ok=True)
os.makedirs(td_Path,exist_ok=True)

def TranslateParsed():
	#	1.	load parsed filed
	Parsed = LoadAllFiles(p_Path)
	Translation = LoadAllFiles(td_Path)

	#	2.	look for stuff which has to be translated
	for fp,data in Parsed.items():
		try:
			ToTranslate=[]
			if fp not in Translation:
				Translation[fp]={}
			c_trans = Translation[fp]

			for i,item in enumerate(data['_items']):
				if str(item['id']) not in c_trans:
					c_trans[str(item['id'])] = {}
				for key, val in item.items():
					if key not in c_trans[str(item['id'])] and val and type(val)==str and CheckKanji(val):
						ToTranslate.append((i,key,val, str(item['id'])))

			#	Translate Data
			#if len(ToTranslate) == 0:
			#	continue

			translated_array = Translate([string for i,key,string,_id in ToTranslate])
			
			#	Write Translation Patch file
			for i,key,jp,_id in ToTranslate:
				if _id not in Translation[fp]:
					Translation[fp][_id]={}
				Translation[fp][_id][key]=translated_array.pop(0)
			open(os.path.join(td_Path,fp),'wb').write(json.dumps(Translation[fp], ensure_ascii=False, indent='\t').encode('utf8'))

			# Write Translated File
			for item in data['_items']:
				item.update(Translation[fp][str(item['id'])])
			open(os.path.join(t_Path,fp),'wb').write(json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))
		except Exception as e:
			print('Error',e)
		
def CheckKanji(string):
	for c in string:
		num = ord(c)
		if num > 0x4E00 - 1 and num < 0x9FA5 + 1:  # range for kanji characters
			return True
	return False		

def LoadAllFiles(path):
	return {fp:json.loads(open(os.path.join(path,fp),'rb').read()) for fp in os.listdir(path)}


total_characters=0
total_requests=0
def TranslateRequest(text):
	global total_characters, total_requests, translator
	try:
		total_characters+=len(text)
		total_requests+=1
		print(time.ctime(), total_requests, total_characters)
		return translator.translate(text, src='ja',dest='en')
	except Exception as e:
		print(e)
		return TranslateRequest(text)
		raise

def Translate(array):
	#reduce array by dupes
	tset = list(set(array))

	seperator = '\r\n'
	strings=[]
	tstrings=[]
	length=0
	for entry in tset:
		if length + len(entry) > 1000 or len(strings) > 200:
			tstrings.extend(TranslateRequest(seperator.join(strings)).text.split(seperator))
			length=0
			strings=[]
		strings.append(entry)
		length += len(entry)
	tstrings.extend(TranslateRequest(seperator.join(strings)).text.split(seperator))

	#	transform back
	
	translation={tset[i]:tstrings[i] for i in range(len(tset))}
	translated=[translation[entry] for entry in array]

	return translated

if __name__ == '__main__':
	TranslateParsed()