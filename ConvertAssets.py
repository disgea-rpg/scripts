import os
import time

#from lib import  MultiThreadHandler, listFiles
import json
import unitypack
from unitypack.unityfolder import UnityFolder

PATH=os.path.dirname(os.path.realpath(__file__))
PATH_ASSETS=os.path.join(PATH,'assets')

debug = True
raw_path=os.path.join(PATH_ASSETS,'raw')
dest_path=os.path.join(PATH_ASSETS,'unpacked2')
originFolder=os.path.join(raw_path,*['Assetbundle'])
destFolder = os.path.join(dest_path,'assetbundle')

def listFiles(directory='',full_path=False):
	return [
		val for sublist in [
			[
				os.path.join(dirpath, filename) if full_path else os.path.join(dirpath, filename)[len(directory)+1:]
				for filename in filenames
			]
			for (dirpath, dirnames, filenames) in os.walk(directory)
			if '.git' not in dirpath
		]
		for val in sublist
	]


def Convert():
	uf = UnityFolder(originFolder,destFolder)
	uf.export()
	input('Finished')
	#mt = MultiThreadHandler()
	####	ASSETBUNDLE	###########################################
	print('Loading')
	for passet in listFiles(originFolder, False):
		print(time.ctime())
		fp = os.path.join(originFolder,passet)
		f = open(fp,'rb')
		b = unitypack.load(f)
		a = b.assets[0]
		a.export('Test')
		print(time.ctime())
		input()
		#processUnityFile(**{'f':fp,'assetPath':passet,'destFolder':destFolder,'copy':False})
		#mt.queue.put((processUnityFile,{'f':fp,'assetPath':passet,'destFolder':destFolder,'copy':False}))
	print('Processing')
	#mt.RunThreads()

if __name__ == '__main__':
	Convert()