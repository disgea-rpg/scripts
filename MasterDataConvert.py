import os, json, time
import msgpack
from Translation import TranslateParsed

#	AES
from Crypto.Cipher import AES
AesBlockSize = 128
AesKeySize = 256
AesIVSize = 16
AesInitVector = b'aO5yK7tOlu1XkKTS'
AesKey = b'Ze3DZ8mKkovba/mYz7Z/AUkguQfEvnnM'.ljust(16, b"\0")

#	Settings
UNPACK = True

def main():
	#	unpack data
	e_path = 'masterdata\\encrypted'
	d_path = 'masterdata\\parsed'
	os.makedirs(d_path, exist_ok=True)
	if UNPACK:
		for fp in os.listdir(e_path):
			try:
				Extract(os.path.join(e_path,fp),os.path.join(d_path,fp+'.json'))
			except Exception as e:
				print('Failed to extract',fp,e )

	#	translate data
	t_path = 'masterdata\\translation'
	# os.makedirs(t_path,exist_ok=True)
	# for fp in os.listdir(d_path):
	# 	Translate(os.path.join(d_path,fp),os.path.join(t_path,fp))

	#	create patch
	p_path = 'masterdata\\patch'
	os.makedirs(p_path,exist_ok=True)
	for fp in os.listdir(t_path):
		data = open(os.path.join(t_path,fp),'rb').read()
		data = json.loads(data)
		open(os.path.join(p_path,fp[:-5]),'wb').write(Encrypt(Pack(data)))


def Extract(origin,dest):
	data = open(origin, 'rb').read()
	ddata = Decrypt(data)
	udata = Unpack(ddata)
	open(dest,'wb').write(json.dumps(udata, ensure_ascii=False, indent='\t').encode('utf8'))

def DataCheck(origin):
	with open(origin, 'rb') as f:
		data = f.read()
	ddata = Decrypt(data)
	udata = Unpack(ddata)
	pdata = Pack(udata)

def Encrypt(data):
	#	zeroes padding
	data+=b'\x00'*(16-len(data)%16)
	cipher = AES.new(AesKey, AES.MODE_CBC, AesInitVector)
	return cipher.encrypt(data)

def Decrypt(data):
	cipher = AES.new(AesKey, AES.MODE_CBC, AesInitVector)
	return cipher.decrypt(data).rstrip(b'\x00')

def Unpack(data):
	return msgpack.unpackb(data,raw=False)

def Pack(data):
	return msgpack.packb(data)

if __name__ == "__main__":
	main()